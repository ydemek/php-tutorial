<?php 
class Cars{
    var $wheels = 4;
    function details(){
        return $this->wheels;
    }
}
class Trucs extends Cars{
    var $wheels = 10;
} 
$car = new Cars();
$truc = new Trucs();
echo "Cars have " . $car->details() . " wheels.<br>";
echo "Trucs have " . $truc->details() . " wheels.<br>";
?>