<?php
class Cars {
    public $wheels_count = 4;
    private $mirrors_count = 2;
    protected $doors_count = 3;

    function car_details(){
        echo $this->wheels_count . "<br>";
        echo $this->mirrors_count . "<br>";
        echo $this->doors_count . "<br>";
    }


}
$car = new Cars();
//echo "Out side of the class public" . $car->wheels_count . "<br>"; 
//echo "Out side of the class private" . $car->mirrors_count . "<br>" ;
//echo "Out side of the class protected" . $car->doors_count . "<br>";

$car->car_details();
?>